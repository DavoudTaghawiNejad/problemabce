from __future__ import division
import abce


class Household(abce.Agent, abce.Household):
    def init(self, simulation_parameters, agent_parameters):
        self.num_capital_firms = simulation_parameters['capitalfirm']
        self.create('ball', self.num_capital_firms)

    def sell_labor(self):
        for i in range(self.num_capital_firms):
            print self.possession('ball')
            self.sell('capitalfirm', i, good='ball', quantity=1, price=0)

    def buy_good(self):
        offers = self.get_offers('ball')
        for offer in offers:
            self.accept(offer)

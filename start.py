from __future__ import division
import multiprocessing as mp
from capitalfirm import CapitalFirm
from goodfirm import GoodFirm
from household import Household
from abce import Simulation, read_json_parameters, repeat

def main():
    for simulation_parameters in read_json_parameters('simulation_parameters.json'):
        s = Simulation(simulation_parameters)
        action_list = [
            ('household', 'sell_labor'),
            ('capitalfirm', 'buy_labor'),
            ('capitalfirm', 'sell_capital'),
            ('goodfirm', 'buy_capital'),
            ('goodfirm', 'sell_good'),
            ('household', 'buy_good')]
        s.add_action_list(action_list)

        s.declare_perishable(good='labor')

        s.aggregate('household')
        s.aggregate('firm')

        s.build_agents(CapitalFirm, simulation_parameters['capitalfirm'])
        s.build_agents(GoodFirm, simulation_parameters['goodfirm'])
        s.build_agents(Household, 1)


        s.run()

if __name__ == '__main__':
    mp.freeze_support()
    main()


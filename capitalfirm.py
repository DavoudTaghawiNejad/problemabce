from __future__ import division
import abce


class CapitalFirm(abce.Agent, abce.Firm):
    def init(self, simulation_parameters, agent_parameters):
        self.num_good_firms = simulation_parameters['goodfirm']

    def buy_labor(self):
        offers = self.get_offers('ball')
        for offer in offers:
            self.accept(offer)

    def sell_capital(self):
        quantity = self.possession('ball') / self.num_good_firms
        for i in range(self.num_good_firms):
            self.sell('goodfirm', i, good='ball', quantity=quantity, price=0)

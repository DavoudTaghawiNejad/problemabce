from __future__ import division
import abce


class GoodFirm(abce.Agent, abce.Firm):
    def init(self, simulation_parameters, agent_parameters):
        pass

    def buy_capital(self):
        offers = self.get_offers('ball')
        for offer in offers:
            self.accept(offer)

    def sell_good(self):
        self.sell('household', 0,
                  good='ball',
                  quantity=self.possession('ball'),
                  price=0)
